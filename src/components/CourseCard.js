import { useState } from 'react';


/* ACTIVITY S50 */
import {Button, Card} from 'react-bootstrap';

export default function CourseCard({course}) {
	// const {properties} = course
	const {name, description, price} = course;
	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	/*
		SYNTAX
		const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0);
	console.log(useState);

	function enroll() {
		setCount(count + 1);
	}


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Card.Text>Enrollees: {count}</Card.Text>
	        {/*<Button variant="primary">Enroll</Button>*/}
	        <Button className="bg-primary" onClick={enroll}>Enroll</Button>
	    </Card.Body>
	</Card>
	)
}